#!/bin/bash

docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml build

docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml down
docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml up -d

docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml exec -T php php bin/console cache:clear -n

docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml exec -T php php bin/console doctrine:migrations:migrate -n

if [ "fixtures" = "$1" ] ; then
  docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml exec -T php php bin/console hautelook:fixtures:load --purge-with-truncate -n
fi
